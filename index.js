const express = require('express')
const { Pool, Client } = require('pg')
const mysql = require('mysql')
const moment = require('moment')

const mapVariableCode = {
    //teambuilding
    fromDate: 'fromDate',
    toDate: 'toDate',
    content: 'reason',
    amountPeople: 'team_size',
    cost: 'cost',
    comment: 'note',
    //update working hours
    checkOutTime:'check-out_time',
    checkInTime: 'check-in_time',
    reason: 'reason',
    //request for supply
    listProduct: 'quotation',
    amount: 'amount',
    //request apply for leave
    dateTarget: 'listDate',
    //request for payment
    typePayment: 'type',
    recipient:'receiver',
    attachment:'attach',
    amountMoney:'cost'
}

const app = express()
const PORT = 8080

app.get('/',(req,res)=>{
    res.send('Hello node')
})

app.listen(PORT, ()=>{console.log(`app is connect with port ${PORT}`)})

const conn = mysql.createConnection({
    database: 'hrsol',
    host: 'localhost',
    user:'root',
    password:'123456789'
})

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'camunda',
    password: '123456789',
    port: 5432,
})

//INSERT USER TABLE
// conn.connect(async (err)=>{
//     if(err) throw err
//     console.log('database mysql connected');
//     var sql = `SELECT u.email, u.first_name, u.last_name `+
//     ` FROM hrsol.user u` +
//     ` where u.email is not null and u.email <> '' `+
//     ` and u.email not in (`+
//     ` select u2.email from hrsol.user u2 group by u2.email`+
//     ` having count(u2.email) > 1)`
    
//     await conn.query(sql, async (err, results)=>{
//         if(err) throw err
        
//         for(let element of results){
//             let temp = {}
//             if(element.email){
//                  temp = {...element, email:element.email.replace(/[@.]/g,''), fullName: `${element.first_name} ${element.last_name}`}
//             }
//             else{
//                 temp = {...element, email:null, fullName: `${element.first_name} ${element.last_name}`}
//             }
//             const text = 'insert into act_id_user(id_, rev_, first_) values($1, $2, $3)'
//             const values = [temp.email, 1, temp.fullName]
//             pool.query(text, values, (err, res) => {
//                 if(err) throw err
//             })        
//         }
//         console.log('postgresql connected');
//     })
// })


//INSERT act_hi_procinst TABLE
// conn.connect(async (err)=>{
//     if(err) console.log(err);
//     console.log('database mysql connected');
//     var sql = 'select tb1.id, tb2.business_name, tb1.created_at, u.username, u.email, tb1.complete, tb1.status from business_registration tb1 left join business_organization_workflow tb2 on tb1.business_organization_workflow_id = tb2.id left join user u on tb1.user_id = u.id'
//     await conn.query(sql, async (err, results)=>{
//         if(err) console.log(err);
//         for(let element of results){
//             let temp = {...element}

//             let checkTypeRequest = temp.business_name.toLowerCase()
//             if(checkTypeRequest.includes('building')){
//                 temp.business_name='request_for_team_building'
//             }
//             else if(checkTypeRequest.includes('hours') || checkTypeRequest.includes('chấm công')){
//                 temp.business_name='update_working_hours'
//             }
//             else if(checkTypeRequest.includes('leave') || checkTypeRequest.includes('phép')){
//                 temp.business_name='apply_for_leaving'
//             }
//             else if(checkTypeRequest.includes('payment') || checkTypeRequest.includes('thanh') || checkTypeRequest.includes('mua')){
//                 temp.business_name='payment'
//             }
//             else if(checkTypeRequest.includes('over')){
//                 temp.business_name='over_time_request'
//             }
//             else if(checkTypeRequest.includes('supply')){
//                 temp.business_name='request_for_supply'
//             }

//             temp.start_time_ = (moment(temp.created_at).format('yyyy-MM-DD HH:mm:ss'))
//             temp.proc_def_id_ = `${temp.business_name}:${temp.id}`

//             if(element.email){
//                 temp = {...temp, email:element.email.replace(/[@.]/g,'')}
//             }
//             else{
//                 temp = {...temp, email:null}
//             }
            
//             let duration = 1;
//             if(temp.complete === 0){
//                 duration = 0
//             }
//             else {
//                 let checkStatus = temp.status.toLowerCase()
//                 if(checkStatus.includes('không') || checkStatus.includes('not')) {
//                     duration = 0
//                 }
//             }

//             const findProcKeyId = `select id_ 
//             from act_re_procdef
//             where key_ = '${temp.business_name}'`

//             await pool.query(findProcKeyId, (err,res)=>{
//                 const text = 'insert into act_hi_procinst(id_, proc_inst_id_, proc_def_key_, proc_def_id_, start_time_, end_time_, duration_, start_user_id_, start_act_id_, end_act_id_, root_proc_inst_id_, state_) values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)'
//                 const values = [temp.id, temp.id, temp.business_name, res.rows[0].id_, temp.start_time_, moment().format('YYYY-MM-DD hh:mm:ss'), duration, temp.email, 'StartEvent_1', `Event_${temp.id}`, temp.id, 'COMPLETED']
//                 pool.query(text, values, (err, res) => {
//                     if(err) throw err
//                 })      
//             })
//         }
//         console.log('postgresql connected');
//     })
// })


//INSERT act_hi_tasknist table
// conn.connect(async (err)=>{
//     if(err) throw err
//     console.log('database mysql connected');
//     var sql = 'select tb1.id, tb2.business_name, tb1.created_at,  u.username, tb1.complete, tb1.status from business_registration tb1 left join business_organization_workflow tb2 on tb1.business_organization_workflow_id = tb2.id left join user u on tb1.user_id = u.id'
//     await conn.query(sql, async (err, results)=>{
//         if(err) throw err
//         for(let element of results){
//             let temp = {...element}
//             let checkTypeRequest = temp.business_name.toLowerCase()
//             if(checkTypeRequest.includes('building')){
//                 temp.business_name='request_for_team_building'
//             }
//             else if(checkTypeRequest.includes('hours') || checkTypeRequest.includes('chấm công')){
//                 temp.business_name='update_working_hours'
//             }
//             else if(checkTypeRequest.includes('leave') || checkTypeRequest.includes('phép')){
//                 temp.business_name='apply_for_leaving'
//             }
//             else if(checkTypeRequest.includes('payment') || checkTypeRequest.includes('thanh') || checkTypeRequest.includes('mua')){
//                 temp.business_name='payment'
//             }
//             else if(checkTypeRequest.includes('over')){
//                 temp.business_name='over_time_request'
//             }
//             else if(checkTypeRequest.includes('supply')){
//                 temp.business_name='request_for_supply'
//             }
//             temp.proc_def_id_ = `${temp.business_name}:${temp.id}`
//             temp.start_time_ = (moment(temp.created_at).format('yyyy-MM-DD HH:mm:ss'))

//             const findProcKeyId = `select id_ 
//             from act_re_procdef
//             where key_ = '${temp.business_name}'`

//             pool.query(findProcKeyId, (err,res)=>{
//                 const text = 'insert into act_hi_taskinst(id_, task_def_key_, proc_def_key_, proc_def_id_, root_proc_inst_id_, proc_inst_id_, execution_id_, act_inst_id_,  name_, assignee_, start_time_, end_time_, duration_, delete_reason_, priority_) values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)'
//                 const values = [temp.id, temp.id, temp.business_name, res.rows[0].id_, temp.id, temp.id, temp.id, temp.proc_def_id_ , 'HR Review', 'kimliennguyenthi1citynowvn', temp.start_time_, temp.start_time_, 1, 'completed', 50]
                
//                 pool.query(text, values, (err, res) => {
//                     if(err) throw err
//                 })
//             })
//         }
//         console.log('successful :>> ');
//     })
// })


//INSERT act_hi_varinst table
// conn.connect(async (err)=>{
//     if(err) throw err
//     console.log('database mysql connected');
//     var sql = 'select tb1.id, tb1.name, tb1.code, tb2.business_name, tb1.created_at,  u.username, tb1.complete, tb1.status from business_registration tb1 left join business_organization_workflow tb2 on tb1.business_organization_workflow_id = tb2.id left join user u on tb1.user_id = u.id'
//     await conn.query(sql, async (err, results)=>{
//         if(err) throw err
//         for(let element of results){
//             let temp = {...element}
//             let checkTypeRequest = temp.business_name.toLowerCase()
//             if(checkTypeRequest.includes('building')){
//                 temp.business_name='request_for_team_building'
//             }
//             else if(checkTypeRequest.includes('hours') || checkTypeRequest.includes('chấm công')){
//                 temp.business_name='update_working_hours'
//             }
//             else if(checkTypeRequest.includes('leave') || checkTypeRequest.includes('phép')){
//                 temp.business_name='apply_for_leaving'
//             }
//             else if(checkTypeRequest.includes('payment') || checkTypeRequest.includes('thanh') || checkTypeRequest.includes('mua')){
//                 temp.business_name='payment'
//             }
//             else if(checkTypeRequest.includes('over')){
//                 temp.business_name='over_time_request'
//             }
//             else if(checkTypeRequest.includes('supply')){
//                 temp.business_name='request_for_supply'
//             }
//             temp.proc_def_id_ = `${temp.business_name}:${temp.id}`
//             temp.start_time_ = (moment(temp.created_at).format('yyyy-MM-DD HH:mm:ss'))
            
//             let duration = 1;
//             if(Number(temp.complete.toString('hex')) === 0){
//                 duration = 0
//             }
//             else {
//                 let checkStatus = temp.status.toLowerCase()
//                 if(checkStatus.includes('không') || checkStatus.includes('not')) {
//                     duration = 0
//                 }
//             }

//             const findProcKeyId = `select id_ 
//             from act_re_procdef
//             where key_ = '${temp.business_name}'`

//             pool.query(findProcKeyId, (err,res)=>{
//                 const allValuesQuery = `select tb1.id, tb1.name, tb1.code, tb2.business_name, tb1.created_at,  u.username, tb1.complete, tb1.status, tb3.data, tb3.variable_code
//                 from business_registration tb1
//                 left join business_organization_workflow tb2 on tb1.business_organization_workflow_id = tb2.id
//                 left join user u on tb1.user_id = u.id
//                 left join workflow_business_registration_data tb3
//                 on tb3.business_registration_id = tb1.id
//                 where tb1.id = '${temp.id}'`
                
//                 let proc_def_id_value = res.rows
//                 conn.query(allValuesQuery, proc_def_id_value, (err, results)=>{
//                     if(err) throw err
//                     const text = 'insert into act_hi_varinst(id_, proc_def_key_, proc_def_id_, root_proc_inst_id_, proc_inst_id_, execution_id_, act_inst_id_,  name_, var_type_, create_time_, rev_, long_, text_, state_) values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)'
//                     const values = [
//                         [temp.id, temp.business_name, res.rows[0].id_, temp.id, temp.id, temp.id, temp.id, 'code', 'string', temp.start_time_, 0, null, temp.code ,'CREATED'],
//                         [`${temp.id}-2`, temp.business_name, res.rows[0].id_, temp.id, temp.id, temp.id, temp.id, 'title', 'string', temp.start_time_, 0, null, temp.name ,'CREATED'],
//                         [`${temp.id}-3`, temp.business_name, res.rows[0].id_, temp.id, temp.id, temp.id, temp.id, 'response', 'boolean', temp.start_time_, 1, duration, null ,'CREATED']
//                     ]
//                     for(let i=0;i<results.length;i++){
//                         let name = mapVariableCode[results[i].variable_code] || 'test'
//                         let data = results[i].data
//                         if (name === 'listDate'){
//                             // data = results[i].data.replace(/[^a-zA-Z0-9-]/, "")
//                             // data = data + ' 00:00:00'
//                         }
//                         else if (name === 'check-out_time' || name === 'check-in_time'){
//                             data = results[i].data.replace(/"/g, "")
//                         }
//                         let value = [`${temp.id}-${4+i}..`, temp.business_name, res.rows[0].id_, temp.id, temp.id, temp.id, temp.id, name, 'string', temp.start_time_, 1, duration, data ,'CREATED']
//                         values.push(value)
//                     }

//                     for(let i = 0; i<values.length;i++){
//                         pool.query(text, values[i], (err, res) => {
//                             if(err) throw err
//                         })           
//                     }
//                 })
            
//                 // const text = 'insert into act_hi_varinst(id_, proc_def_key_, proc_def_id_, root_proc_inst_id_, proc_inst_id_, execution_id_, act_inst_id_,  name_, var_type_, create_time_, rev_, long_, text_, state_) values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)'
//                 // const values = [
//                 //     [temp.id, temp.business_name, res.rows[0].id_, temp.id, temp.id, temp.id, temp.id, 'code', 'string', temp.start_time_, 0, null, temp.code ,'CREATED'],
//                 //     [`${temp.id}-2`, temp.business_name, res.rows[0].id_, temp.id, temp.id, temp.id, temp.id, 'title', 'string', temp.start_time_, 0, null, temp.name ,'CREATED'],
//                 //     [`${temp.id}-3`, temp.business_name, res.rows[0].id_, temp.id, temp.id, temp.id, temp.id, 'response', 'boolean', temp.start_time_, 1, duration, null ,'CREATED']
//                 // ]
//                 // for(let i = 0; i<values.length;i++){
//                 //     pool.query(text, values[i], (err, res) => {
//                 //         if(err) throw err
//                 //     })           
//                 // }
//             })
//         }
//     console.log('postgresql connected');
//     })
// })

